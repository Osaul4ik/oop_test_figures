public class Circle extends Figures {
    int radius;


    Circle(int r) {
        radius = r;
    }

    public void showParameters() {
        System.out.println("Радиус = " + radius + " см;");
        System.out.println("Площадь круга = " + this.findArea() + " см^2;");
        System.out.println("Периметр круга = " + this.findPerimeter() + " см;");
        this.printPosition();
    }

    public double findArea() {
        double area = Math.PI * Math.pow(radius, 2);
        area = Math.round(area * 10d) / 10d;
        return area;
    }

    public double findPerimeter() {
        double perimeter = 2 * Math.PI * radius;
        perimeter = Math.round(perimeter * 10d) / 10d;
        return perimeter;
    }

    public int findPosition() {
        int xy = radius;
        return xy;
    }

    public void printPosition() {
        System.out.println("Координаты круга: " + "x = " +
                this.findPosition() + "; y = " + this.findPosition() + ";");
    }
}
