public class Figures {
    private String color;
    private int border;

    public void enterFigureParameters(int border, String color) {
        this.border = border;
        this.color = color;
    }

    public void showFigureParameters() {
        System.out.println("Цвет: " + color + "; Размер границы = " + border + "см;");
    }

}


