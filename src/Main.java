public class Main {

    public static void main(String[] args) {
        Triangle firstTriangle = new Triangle(5, 8, 10);
        Square firstSquare = new Square(5);
        Circle firstCircle = new Circle(4);
        Rectangle firstRectangle = new Rectangle(5, 10);

        firstCircle.enterFigureParameters(1, "Red");
        firstSquare.enterFigureParameters(1, "White");
        firstTriangle.enterFigureParameters(1, "Green");
        firstRectangle.enterFigureParameters(1, "Blue");

        System.out.println("---------------------------------------");
        firstTriangle.showParameters();
        firstTriangle.showFigureParameters();
        System.out.println("---------------------------------------");
        firstSquare.showParameters();
        firstTriangle.showFigureParameters();
        System.out.println("---------------------------------------");
        firstCircle.showParameters();
        firstCircle.showFigureParameters();
        System.out.println("---------------------------------------");
        firstRectangle.showParameters();
        firstRectangle.showFigureParameters();
        System.out.println("---------------------------------------");
    }
}
