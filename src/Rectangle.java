public class Rectangle extends Figures {
    int side, top;

    Rectangle(int s, int t) {
        side = s;
        top = t;
    }

    public void showParameters() {
        System.out.println("Сторона a прямоугольника = " + side +
                " см; Сторона b прямоугольника = " + top + " см;");
        System.out.println("Диагональ прямоугольника = " + this.findDiagonal() + " см;");
        System.out.println("Периметр прямоугольника = " + this.findPerimeter() + " см;");
        System.out.println("Площадь прямоугольника = " + this.findArea() + " см^2;");
        this.printPosition();
    }

    public int findPerimeter() {
        int perimeter = side * 2 + top * 2;
        return perimeter;
    }

    public int findArea() {
        int area = side * top;
        return area;
    }

    public double findDiagonal() {
        double diagonal = Math.sqrt(Math.pow(side, 2) + Math.pow(top, 2));
        diagonal = Math.round(diagonal * 10d) / 10d;
        return diagonal;
    }

    public int[][] findPosition() {
        int[][] position = new int[4][2];
        position[0][0] = 0;  //точка x0 [x][]
        position[0][1] = 0; //точка x0 [][y]
        position[1][0] = top; //точка x1 [x][]
        position[1][1] = 0; //точка y1 [][y]
        position[2][0] = 0; //точка x2 [x][]
        position[2][1] = side; //точка y2 [][y]
        position[3][0] = top; //точка x3 [x][]
        position[3][1] = side; //точка y3 [][y]
        return position;
    }

    public void printPosition () {
        int[][] position = this.findPosition();
        System.out.println("Координаты прямоугольника:");
        for (int i = 0; i < position.length; i++) {
            for (int j = 0; j < position[i].length; j++) {
                String coordinate;
                if (j == 0) {
                    coordinate = "x";
                } else {
                    coordinate = "y";
                }
                System.out.print(coordinate + i + " = " + position[i][j] + "; ");
            }
            System.out.println();
        }
    }
}
