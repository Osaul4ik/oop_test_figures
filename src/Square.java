public class Square extends Figures {
    int side;

    Square(int s) {
        side = s;
    }

    public void showParameters() {
        System.out.println("Сторона квадрата = " + side);
        System.out.println("Периметр квадрата = " + this.findPerimeter() + " см;");
        System.out.println("Площадь квадрата = " + this.findArea() + " см^2;");
        System.out.println("Диагональ квадрата = " + this.findDiagonal() + " см;");
        printPosition();
    }

    public int findPerimeter() {
        int perimeter = side * 4;
        return perimeter;
    }

    public double findArea() {
        double area = Math.pow(side, 2);
        return area;
    }

    public double findDiagonal() {
        double diagonal = side * Math.sqrt(2);
        diagonal = Math.round(diagonal * 10d) / 10d;
        return diagonal;
    }

    public int[][] findPosition() {
        int[][] position = new int[4][2];
        position[0][0] = 0;  //точка x0 [x][]
        position[0][1] = 0; //точка x0 [][y]
        position[1][0] = side; //точка x1 [x][]
        position[1][1] = 0; //точка y1 [][y]
        position[2][0] = 0; //точка x2 [x][]
        position[2][1] = side; //точка y2 [][y]
        position[3][0] = side; //точка x3 [x][]
        position[3][1] = side; //точка y3 [][y]
        return position;
    }

    public void printPosition () {
        int[][] position = this.findPosition();
        System.out.println("Координаты квадрата:");
        for (int i = 0; i < position.length; i++) {
            for (int j = 0; j < position[i].length; j++) {
                String coordinate;
                if (j == 0) {
                    coordinate = "x";
                } else {
                    coordinate = "y";
                }
                System.out.print(coordinate + i + " = " + position[i][j] + "; ");
            }
            System.out.println();
        }
    }
}
