public class Triangle extends Figures {
    int left, right, bottom;

    Triangle(int l, int r, int b) {
        left = l;
        right = r;
        bottom = b;
    }

    public void showParameters() {
        System.out.println("a = " + left + " см; b = " + right + " см; c = " +
                bottom + " см;");
        System.out.println("Высота треугольника = " + this.findHeight() + " см;");
        System.out.println("Периметр треугольника = " + this.findPerimeter() + " см;");
        System.out.println("Площадь треугольника = " + this.findArea() + " см^2;");
        this.printPosition();
    }

    public int findPerimeter() {
        int perimeter = left + right + bottom;
        return perimeter;
    }

    public double findArea() {
        double p = findPerimeter() / 2;
        double area = Math.sqrt(p * (p - left) * (p - right) * (p - bottom));
        area = Math.round(area * 10d) / 10d;
        return area;
    }

    public double findHeight() {
        double area = this.findArea();
        double height = (2 * area) / left;
        height = Math.round(height * 10d) / 10d;
        return height;
    }

    public int[][] findPosition() {
        int[][] position = new int[3][2];
        position[0][0] = 0;  //точка x0 [x][]
        position[0][1] = 0; //точка x0 [][y]
        if (Math.pow(left, 2) + Math.pow(bottom, 2) == Math.pow(right, 2)) {
            position[1][0] = 0; //точка x1 [x][]
            position[1][1] = left; //точка y1 [][y]
        } else if (Math.pow(right, 2) + Math.pow(bottom, 2) == Math.pow(left, 2)) {
            position[1][0] = bottom; //точка x1 [x][]
            position[1][1] = right; //точка y1 [][y]
        } else {
            position[1][0] = (int) Math.round(Math.sqrt(Math.pow(left, 2) +
                    Math.pow(this.findHeight(), 2))); //точка x1 [x][]
            position[1][1] = (int) Math.round(this.findHeight()); //точка y1 [][y]
        }
        position[2][0] = bottom; //точка x2 [x][]
        position[2][1] = 0; //точка y2 [][y]
        return position;
    }

    public void printPosition() {
        int[][] position = this.findPosition();
        System.out.println("Координаты треугольника:");
        for (int i = 0; i < position.length; i++) {
            for (int j = 0; j < position[i].length; j++) {
                String coordinate;
                if (j == 0) {
                    coordinate = "x";
                } else {
                    coordinate = "y";
                }
                System.out.print(coordinate + i + " = " + position[i][j] + "; ");
            }
            System.out.println();
        }
    }
}
